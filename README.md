Yii2 Core
==========

## Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run
```
php composer.phar require code2magic/yii2-core --prefer-dist
```
or add
```
"code2magic/yii2-core": "*"
```

to the `require` section of your `composer.json` file.

## Configuration

This extension is supposed to be used with [composer-config-plugin].

Else look files for cofiguration example:

* [src/config/common.php]

[composer-config-plugin]:      https://github.com/hiqdev/composer-config-plugin
[src/config/common.php]:       src/config/common.php

## Usage
