<?php
/**
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
return [
    'aliases' => [
        '@code2magic/core' => dirname(__DIR__) .  '/src/',
    ],
    'container' => [
        'definitions' => [
            \yii\i18n\Formatter::class => \code2magic\core\i18n\Formatter::class,
            \yii\db\ActiveQuery::class => \code2magic\core\db\ActiveQuery::class,
        ],
        'singletons' => [
        ],
    ],
];
