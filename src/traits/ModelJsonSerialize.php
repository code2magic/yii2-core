<?php

namespace code2magic\core\traits;

/**
 * Trait ModelJsonSerialize
 *
 * @package code2magic\core\traits
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
trait ModelJsonSerialize
{
    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        $output = [
            'attributes' => [],
            'attribute_labels' => [],
            'attribute_hints' => [],
            'attribute_required' => [],
            'errors' => $this->getErrors(),
        ];
        foreach ($this->activeAttributes() as $attribute) {
            $output['attributes'][$attribute] = $this->$attribute;
            $output['attribute_labels'][$attribute] = $this->getAttributeLabel($attribute);
            $output['attribute_hints'][$attribute] = $this->getAttributeHint($attribute);
            $output['attribute_required'][$attribute] = $this->isAttributeRequired($attribute);
        }
        return $output;
    }
}
