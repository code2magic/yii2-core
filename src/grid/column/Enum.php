<?php

namespace code2magic\core\grid\column;

use yii\grid\DataColumn;
use yii\helpers\ArrayHelper;

/**
 * Class Enum
 * [
 *      'class' => '= \code2magic\core\grid\column\Enum::class,
 *      'attribute' => 'role',
 *      'enum' => User::getRoles()
 * ]
 *
 * @package common\components\grid
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class Enum extends DataColumn
{
    /**
     * @var array List of value => name pairs
     */
    public $enum = [];

    /**
     * @var bool
     */
    public $loadFilterValues = true;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->loadFilterValues && $this->filter === null) {
            $this->filter = $this->enum;
        }
    }

    /**
     * @param mixed $model
     * @param mixed $key
     * @param int $index
     * @return mixed
     */
    public function getDataCellValue($model, $key, $index)
    {
        $value = parent::getDataCellValue($model, $key, $index);
        return ArrayHelper::getValue($this->enum, $value, $value);
    }
}
