<?php

namespace code2magic\core\helpers;

use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;

/**
 * Class TemplateHelper
 *
 * @package code2magic\core\helpers
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class TemplateHelper
{
    /**
     * A list with annotations that are not causing exceptions when not resolved to an annotation class.
     *
     * The names are case sensitive.
     *
     * @var array
     */
    private static $globalIgnoredNames = [
        // Annotation tags
        'Annotation' => true, 'Attribute' => true, 'Attributes' => true,
        /* Can we enable this? 'Enum' => true, */
        'Required' => true,
        'Target' => true,
        // Widely used tags (but not existent in phpdoc)
        'fix' => true, 'fixme' => true,
        'override' => true,
        // PHPDocumentor 1 tags
        'abstract' => true, 'access' => true,
        'code' => true,
        'deprec' => true,
        'endcode' => true, 'exception' => true,
        'final' => true,
        'ingroup' => true, 'inheritdoc' => true, 'inheritDoc' => true,
        'magic' => true,
        'name' => true,
        'toc' => true, 'tutorial' => true,
        'private' => true,
        'static' => true, 'staticvar' => true, 'staticVar' => true,
        'throw' => true,
        // PHPDocumentor 2 tags.
        'api' => true, 'author' => true,
        'category' => true, 'copyright' => true,
        'deprecated' => true,
        'example' => true,
        'filesource' => true,
        'global' => true,
        'ignore' => true, /* Can we enable this? 'index' => true, */
        'internal' => true,
        'license' => true, 'link' => true,
        'method' => true,
        'package' => true, 'param' => true, 'property' => true, 'property-read' => true, 'property-write' => true,
        'return' => true,
        'see' => true, 'since' => true, 'source' => true, 'subpackage' => true,
        'throws' => true, 'todo' => true, 'TODO' => true,
        'usedby' => true, 'uses' => true,
        'var' => true, 'version' => true,
        // PHPUnit tags
        'codeCoverageIgnore' => true, 'codeCoverageIgnoreStart' => true, 'codeCoverageIgnoreEnd' => true,
        // PHPCheckStyle
        'SuppressWarnings' => true,
        // PHPStorm
        'noinspection' => true,
        // PEAR
        'package_version' => true,
        // PlantUML
        'startuml' => true, 'enduml' => true,
        // Symfony 3.3 Cache Adapter
        'experimental' => true
    ];

    /**
     * @param $path
     * @return array
     */
    public static function getTemplates($path)
    {
        $files = FileHelper::findFiles(\Yii::getAlias($path), [
            'only' => ['*.php', '*.twig', '*.tpl',],
            'recursive' => false,
        ]);
        $names = [];
        $orders = [];
        $templates = [];
        foreach ($files as $file) {
            $params = static::parseTmplVars(file_get_contents($file));
            $name = basename($file, '.php');
            $templates[$name] = [
                'sort' => (int)ArrayHelper::getValue($params, 'order', 0),
                'label' => \Yii::t('cmscontent', ArrayHelper::getValue($params, 'label', $name)),
            ];
        }
        ArrayHelper::multisort($templates, ['sort', 'label',]);
        return ArrayHelper::getColumn($templates, 'label');
    }

    private static function parseTmplVars($tmpl_content)
    {
        preg_match_all('#@tmplVar\s*(?P<key>[\w]*)\s*(?P<value>.+)[\s\v]+#', $tmpl_content, $matches);
        return array_combine($matches['key'], $matches['value']);
    }
}
