<?php

namespace code2magic\core\web\actions;

/**
 * Class ErrorAction
 * @package code2magic\core\web\actions
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class ErrorAction extends \yii\web\ErrorAction
{
    protected function renderHtmlResponse()
    {
        $this->controller->getView()->title = $this->getExceptionName();
        return parent::renderHtmlResponse();
    }
}
