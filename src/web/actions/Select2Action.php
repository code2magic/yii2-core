<?php

namespace code2magic\core\web\actions;

use yii\helpers\ArrayHelper;

/**
 * Action for Select2 ajax data source
 *
 * ```php
 * public function actions()
 * {
 *     $actions = parent::actions();
 *     $actions['list'] = [
 *         'class' => \code2magic\core\web\actions\Select2Action::class,
 *         'getQuery' => function($words){
 *             return Model1::find();
 *         },
 *         'prepareId' => 'key',
 *         'prepareText' => 'prop1',
 *     ];
 *     // with callable and injection
 *     $actions['list2'] = [
 *         'class' => \code2magic\core\web\actions\Select2Action::class,
 *         'getQuery' => function(\yii\web\Request $request, $words){
 *              return Model2::find()
 *                  ->filterWhere([
 *                      'and',
 *                      ['like', 'prop1', $words],
 *                      ['prop3' => $request->getQueryParam('param'),],
 *                  ]);
 *         },
 *         'prepareId' => function($model){
 *             return $model->id;
 *         },
 *         'prepareText' => function($model){
 *             return "{$model->prop1} ({$model->prop2})";
 *         },
 *     ];
 *     // with callable to prepare custom row
 *     $actions['list2'] = [
 *         'class' => \code2magic\core\web\actions\Select2Action::class,
 *         'getQuery' => function($words){
 *              return Model2::find()
 *                  ->filterWhere([
 *                      'and',
 *                      ['like', 'prop1', $words],
 *                  ]);
 *         },
 *         'prepareRow' => function($model){
 *             return ['id' => $model->id, 'text' => $model->text, 'custom_attribute' => $model->custom_attribute,];
 *         },
 *     ];
 *     return $actions;
 * }
 * ```
 *
 * @package code2magic\core\web\actions
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class Select2Action extends \yii\base\Action
{
    /**
     * @var int
     */
    public $limit = 20;

    /**
     * @var callable
     */
    public $getQuery;

    /**
     * @var string|callable
     */
    public $prepareId = 'id';

    /**
     * @var string|callable
     */
    public $prepareText = 'text';

    /**
     * @var null|callable
     */
    public $prepareRow;

    /**
     * @param string|null $q
     * @param int $page
     * @param string|null $limit
     * @return \yii\web\Response
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function run($q = null, $page = 1, $limit = null)
    {
        $output = ['results' => [], 'pagination' => ['more' => false,],];
        $limit = !empty($limit) ? $limit : $this->limit;
        $offset = ($page - 1) * $limit;
        if (is_callable($this->getQuery)) {
            /** @var  $query \yii\db\Query */
            $query = \Yii::$container->invoke($this->getQuery, [preg_split('/[\s,.]+/', $q, null, PREG_SPLIT_NO_EMPTY),]);
            $output['pagination']['more'] = ($query->count() / $limit) > $page;
            $query->limit($limit)->offset($offset);
            foreach ($query->all() as $item) {
                if (is_callable($this->prepareRow)) {
                    $output['results'][] = \Yii::$container->invoke($this->prepareRow, [$item,]);
                } else {
                    $output['results'][] = [
                        'id' => is_callable($this->prepareId) ? \Yii::$container->invoke($this->prepareId, [$item,]) : ArrayHelper::getValue($item, $this->prepareId),
                        'text' => is_callable($this->prepareText) ? \Yii::$container->invoke($this->prepareText, [$item,]) : ArrayHelper::getValue($item, $this->prepareText),
                    ];
                }
            }
        }
        return $this->controller->asJson($output);
    }
}
