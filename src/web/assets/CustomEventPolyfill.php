<?php

namespace code2magic\core\web\assets;

/**
 * Class CustomEventPolyfill
 * @package code2magic\core
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class CustomEventPolyfill extends \yii\web\AssetBundle
{
    public $sourcePath = '@vendor/npm-asset/custom-event-polyfill';

    /**
     * @var array
     */
    public $js = [
        'polyfill.js',
    ];
}
