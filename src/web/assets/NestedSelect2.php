<?php

namespace code2magic\core\web\assets;

/**
 * Class NestedSelect2
 * @package code2magic\core
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class NestedSelect2 extends \yii\web\AssetBundle
{
    /**
     * @inheritDoc
     */
    public $sourcePath = __DIR__ . '/src';

    /**
     * @inheritDoc
     */
    public $js = [
        'js/nested-select2.js',
    ];

    /**
     * @inheritDoc
     */
    public $depends = [
        \yii\web\YiiAsset::class,
    ];

    /**
     * @return \yii\web\JsExpression
     */
    public static function getSelect2DataFunction()
    {
        return new \yii\web\JsExpression(<<<JS
function (params) {
    params.q = params.term;
    var parent_element = $($(this).data("parent_element"));
    if (parent_element.length > 0){
        var parent_param = $(this).data("parent_param") ? $(this).data("parent_param") : "id";
        params[parent_param] = parent_element.val();
    }
    return params;
}
JS
        );
    }
}
