<?php

namespace code2magic\core\web\assets;

use yii\web\View;

/**
 * Class Html5shiv
 * @package code2magic\core
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class Html5shiv extends \yii\web\AssetBundle
{
    public $sourcePath = '@vendor/npm-asset/html5shiv/dist';
    public $js = [
        'html5shiv.min.js',
    ];

    public $jsOptions = [
        'condition' => 'lt IE 9',
        'position' => View::POS_HEAD,
    ];
}
