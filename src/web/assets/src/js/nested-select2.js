/**
 * @package code2magic\core
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
(function ($) {
    $(document).on('change', 'select[data-child_element]', function (event) {
        var element = $(event.target);
        $(element.data('child_element'))
            .html('')
            .val('')
            .trigger('change')
            .prop('disabled', !element.val());
    });
})(jQuery);
