/**
 * @package code2magic\core
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
;(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define([], factory(root));
    } else if (typeof exports === 'object') {
        module.exports = factory(root);
    } else {
        root.code2magic = factory(root);
    }
})(typeof global !== 'undefined' ? global : window || this.window || this.global, function (root) {
    'use strict';
    // Dependencies
    var _ = root._;

    // Private Variables
    var
        // Default settings
        DEFAULTS = {
            showMessage: {
                providerDefault: false,
                providers: {}
            },
            timeout: {
                handlerDefault: 100
            }
        },
        // Current settings
        CONFIG = _.extend({}, DEFAULTS),
        templates = {},
        timeouts = {}
    ;

    //
    var $instance = {};

    /**
     *
     * @param tmpl
     * @param params
     * @returns {*}
     */
    $instance.render = function (tmpl, params) {
        if (typeof templates[tmpl] !== 'function') {
            templates[tmpl] = _.template(document.getElementById(tmpl).innerHTML);
        }
        return templates[tmpl](params);
    };

    /**
     *
     * @param message_data
     * @param provider
     */
    $instance.showMessage = function (message_data, provider) {
        if (message_data === undefined) {
            return;
        }
        provider = provider ? provider : CONFIG.showMessage.providerDefault;

        if (typeof CONFIG.showMessage.providers[provider] === 'function') {
            CONFIG.showMessage.providers[provider].call($instance, message_data);
        } else {
            alert(message_data);
        }
    };

    /**
     *
     * @param handler
     * @param key
     * @param wait
     */
    $instance.timeout = function (handler, key, wait){
        wait = typeof wait !== 'undefined' ? wait : CONFIG.timeout.handlerDefault;
        clearTimeout(timeouts[key]);
        timeouts[key] = setTimeout(handler, wait);
    }

    /**
     *
     * @param data
     * @returns {string}
     */
    $instance.encodeQueryData = function (data) {
        var ret = [];
        for (var d in data)
            if (data[d]) {
                ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
            }

        return ret.join('&');
    };

    /**
     *
     * @param options
     */
    $instance.settings = function (options) {
        CONFIG = _.extend({}, DEFAULTS, options || {});
    };

    return $instance;
});
