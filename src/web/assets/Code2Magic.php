<?php

namespace code2magic\core\web\assets;

/**
 * Class Code2Magic
 * @package code2magic\core
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class Code2Magic extends \yii\web\AssetBundle
{
    /**
     * @inheritDoc
     */
    public $sourcePath = __DIR__ . '/src';

    /**
     * @inheritDoc
     */
    public $js = [
        'js/code2magic.js'
    ];
}
