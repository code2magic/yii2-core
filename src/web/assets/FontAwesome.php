<?php

namespace code2magic\core\web\assets;

/**
 * Class FontAwesome
 * @package code2magic\core
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class FontAwesome extends \yii\web\AssetBundle
{
    public $sourcePath = '@vendor/npm-asset/font-awesome';
    public $css = [
        'css/font-awesome.min.css'
    ];
}
