<?php

namespace code2magic\core\web\helpers;

use yii\di\Instance;

/**
 * Class PrepareJsVars
 * @package code2magic\core\web\helpers
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class JsVarsHelper
{
    /**
     * @var array
     */
    public $js_vars = [];

    /**
     * @param \yii\web\View $view
     * @throws \yii\base\InvalidConfigException
     */
    public static function prepare(\yii\web\View $view): void
    {
        /** @var self $JsVarsHelper */
        $JsVarsHelper = Instance::ensure(self::class);
        foreach ($JsVarsHelper->js_vars as $name => $value){
            $view->registerJsVar($name, is_callable($value) ? \Yii::$container->invoke($value, []) : $value);
        }
    }
}
