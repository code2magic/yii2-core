<?php

namespace code2magic\core\web\behaviors;

use yii\base\Behavior;
use yii\web\User;

/**
 * Class LoginTimestampBehavior
 * @package code2magic\core\web\behaviors
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class LoginTimestampBehavior extends Behavior
{
    /**
     * @var string
     */
    public $attribute = 'logged_at';


    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            User::EVENT_AFTER_LOGIN => 'afterLogin'
        ];
    }

    /**
     * @param $event \yii\web\UserEvent
     */
    public function afterLogin($event)
    {
        $user = $event->identity;
        $user->touch($this->attribute);
        $user->save(false);
    }
}
