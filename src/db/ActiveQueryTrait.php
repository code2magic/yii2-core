<?php

namespace code2magic\core\db;

/**
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
trait ActiveQueryTrait
{
    use \yii\db\ActiveQueryTrait;

    /**
     * @var array|null
     */
    static protected $_select_columns;

    /**
     * @return array|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getSelectColumns()
    {
        if (self::$_select_columns === null) {
            self::$_select_columns = [];
            foreach (($this->modelClass)::getTableSchema()->columns as $column) {
                self::$_select_columns[$column->name] = $this->getAlias() . '.' . $column->name;
            }
        }
        return self::$_select_columns;
    }

    /**
     *
     */
    public function getAlias()
    {
        list(, $alias) = $this->getTableNameAndAlias();
        return $alias;
    }
}