<?php

namespace code2magic\core\db;

/**
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class ActiveQuery extends \yii\db\ActiveQuery
{
    use ActiveQueryTrait;
}
