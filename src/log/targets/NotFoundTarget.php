<?php

namespace code2magic\core\log\targets;

use yii\log\Target;

/**
 * Class NotFoundTarget
 * @package code2magic\core\log\targets
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class NotFoundTarget extends Target
{
    /**
     * Exports log [[messages]] to a specific destination.
     * Child classes must implement this method.
     */
    public function export()
    {
        // TODO: Implement export() method.
    }
}
