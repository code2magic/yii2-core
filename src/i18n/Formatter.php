<?php

namespace code2magic\core\i18n;

use Closure;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\di\NotInstantiableException;
use yii\helpers\ArrayHelper;

/**
 * Class Formatter
 *
 * @package code2magic\core\i18n
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class Formatter extends \yii\i18n\Formatter
{
    /**
     * @var string[]
     *
     * ```php
     * [
     *     \NumberFormatter::CURRENCY => '#,##0.# ¤',,
     * ]
     * ```
     */
    public $patterns = [];

    /**
     * @var array
     */
    public $format_types = [];

    /**
     * @inheritDoc.
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    public function format($value, $format)
    {
        if ($format instanceof Closure) {
            return $format($value, $this);
        } elseif (is_array($format)) {
            if (!isset($format[0])) {
                throw new InvalidArgumentException('The $format array must contain at least one element.');
            }
            $f = $format[0];
            $format[0] = $value;
            $params = $format;
            $format = $f;
        } else {
            $params = [$value];
        }

        if (array_key_exists($format, $this->format_types) && is_callable($this->format_types[$format])) {
            return \Yii::$container->invoke($this->format_types[$format], $params);
        }

        $method = 'as' . $format;
        if ($this->hasMethod($method)) {
            return call_user_func_array([$this, $method], $params);
        }

        throw new InvalidArgumentException("Unknown format type: $format");
    }

    /**
     * @inheritDoc
     */
    protected function createNumberFormatter($style, $decimals = null, $options = [], $textOptions = [])
    {
        $formatter = new \NumberFormatter($this->locale, $style);

        // set pattern
        if ($pattern = ArrayHelper::getValue($this->patterns, $style, false)) {
            $formatter->setPattern($pattern);
        }

        // set text attributes
        foreach ($this->numberFormatterTextOptions as $name => $attribute) {
            $formatter->setTextAttribute($name, $attribute);
        }
        foreach ($textOptions as $name => $attribute) {
            $formatter->setTextAttribute($name, $attribute);
        }

        // set attributes
        foreach ($this->numberFormatterOptions as $name => $value) {
            $formatter->setAttribute($name, $value);
        }
        foreach ($options as $name => $value) {
            $formatter->setAttribute($name, $value);
        }
        if ($decimals !== null) {
            $formatter->setAttribute(\NumberFormatter::MAX_FRACTION_DIGITS, $decimals);
            $formatter->setAttribute(\NumberFormatter::MIN_FRACTION_DIGITS, $decimals);
        }

        // set symbols
        if ($this->decimalSeparator !== null) {
            $formatter->setSymbol(\NumberFormatter::DECIMAL_SEPARATOR_SYMBOL, $this->decimalSeparator);
        }
        if ($this->thousandSeparator !== null) {
            $formatter->setSymbol(\NumberFormatter::GROUPING_SEPARATOR_SYMBOL, $this->thousandSeparator);
            $formatter->setSymbol(\NumberFormatter::MONETARY_GROUPING_SEPARATOR_SYMBOL, $this->thousandSeparator);
        }
        foreach ($this->numberFormatterSymbols as $name => $symbol) {
            $formatter->setSymbol($name, $symbol);
        }

        return $formatter;
    }

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    public function __call($name, $params)
    {
        if(mb_strpos($name, 'as') === 0){
            $format = mb_strtolower(mb_substr($name, 2, 1)) . mb_substr($name, 3, null);
            if (array_key_exists($format, $this->format_types) && is_callable($this->format_types[$format])) {
                return \Yii::$container->invoke($this->format_types[$format], $params);
            }
        }
        return parent::__call($name, $params);
    }
}
